package http.content.header;

import http.global.HttpGlobal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class HttpDateParser {
	private static volatile HttpDateParser instance;
	private DateFormat dateFormat;

	public static HttpDateParser getInstance() {
		if (instance == null)
			synchronized (HttpDateParser.class) {
				if (instance == null)
					instance = new HttpDateParser();
			}
		return instance;
	}

	private HttpDateParser() {
		dateFormat = SimpleDateFormat.getTimeInstance();
	}

	/**
	 * Creates a RFC 1123 date format, this can be appended to a http header
	 * directly. It does not contain the "Date: " start of the header line.
	 * 
	 * @return RFC 1123 of the current time of the machine, as a {@link String}.
	 */
	private String getRfc1123() {
		Calendar now = GregorianCalendar.getInstance();
		return String.format("%s, %s %s %s %s GMT", getDayOfWeek(now),
				getDayOfMonth(now), getMonth(now), getYear(now),
				getTimeOfDay(now));
	}

	public byte[] getTimeHeader() {
		return String.format("Date: %s%s", getRfc1123(), HttpGlobal.CRLF)
				.getBytes();
	}

	private String getDayOfWeek(Calendar now) {
		switch (now.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.MONDAY:
			return "Mon";
		case Calendar.TUESDAY:
			return "Tue";
		case Calendar.WEDNESDAY:
			return "Wed";
		case Calendar.THURSDAY:
			return "Thu";
		case Calendar.FRIDAY:
			return "Fri";
		case Calendar.SATURDAY:
			return "Sat";
		case Calendar.SUNDAY:
			return "Sun";
		default:
			throw new IllegalArgumentException(String.format(
					"Day not known: %s", now));
		}
	}

	private String getDayOfMonth(Calendar now) {
		int day_of_month = now.get(Calendar.DAY_OF_MONTH);
		return day_of_month < 10 ? "0" + String.valueOf(day_of_month) : String
				.valueOf(day_of_month);
	}

	private String getMonth(Calendar now) {
		switch (now.get(Calendar.MONTH)) {
		case Calendar.JANUARY:
			return "Jan";
		case Calendar.FEBRUARY:
			return "Feb";
		case Calendar.MARCH:
			return "Mar";
		case Calendar.APRIL:
			return "Apr";
		case Calendar.MAY:
			return "May";
		case Calendar.JUNE:
			return "Jun";
		case Calendar.JULY:
			return "Jul";
		case Calendar.AUGUST:
			return "Aug";
		case Calendar.SEPTEMBER:
			return "Sep";
		case Calendar.OCTOBER:
			return "Oct";
		case Calendar.NOVEMBER:
			return "Nov";
		case Calendar.DECEMBER:
			return "Dec";
		default:
			throw new IllegalArgumentException(String.format(
					"Month not known: %s", now));
		}
	}

	private String getYear(Calendar now) {
		return String.valueOf(now.get(Calendar.YEAR));
	}

	private String getTimeOfDay(Calendar now) {
		return dateFormat.format(now.getTime());
	}
}
