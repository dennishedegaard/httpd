package http.content.header;

import static http.content.HttpContent.arrayToVector;
import static http.content.HttpContent.vectorToArray;
import static http.global.HttpGlobal.CRLF;
import static http.global.HttpGlobal.APP_NAME;
import http.global.HttpGlobal;
import http.log.Log;
import http.utils.mime.ContentType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.Vector;

public class HttpHeader {
	private Socket socket;
	private String path;

	public HttpHeader(Socket socket, String path) {
		this.socket = socket;
		this.path = path;
	}

	private String getIp() {
		return socket.getInetAddress().getHostAddress();
	}

	public byte[] get200() {
		List<Byte> bytes = new Vector<Byte>();
		bytes.addAll(arrayToVector(String.format("HTTP/1.1 200 OK%s", CRLF)
				.getBytes()));
		bytes.addAll(arrayToVector(HttpDateParser.getInstance().getTimeHeader()));
		bytes.addAll(arrayToVector(getServer()));
		bytes.addAll(arrayToVector(getContentType()));
		bytes.addAll(arrayToVector(String.format("Connection: close%s", CRLF)
				.getBytes()));
		return vectorToArray(bytes);
	}

	/**
	 * Returns a code 404.(File not found).
	 * 
	 * @return the byte[] that handles a 404.
	 */
	public byte[] get404() {
		Vector<Byte> bytes = new Vector<Byte>(arrayToVector(String.format(
				"HTTP/1.1 404 Not Found%s", CRLF).getBytes()));
		File f404 = new File(HttpGlobal.ERROR_404);
		bytes.addAll(arrayToVector(HttpDateParser.getInstance().getTimeHeader()));
		bytes.addAll(arrayToVector(getServer()));
		bytes.addAll(arrayToVector(String.format("Content-Type: %s%s",
				ContentType.getInstance().getContentType(f404.getPath()), CRLF)
				.getBytes()));
		bytes.addAll(arrayToVector(String.format("Connection: close", CRLF)
				.getBytes()));
		bytes.addAll(arrayToVector(CRLF.getBytes()));
		if (!f404.exists())
			Log.fine(getIp() + ": 404 document not found.");
		else {
			try {
				FileInputStream in = new FileInputStream(f404);
				int b;
				while ((b = in.read()) != -1)
					bytes.add((byte) b);
				in.close();
			} catch (FileNotFoundException e) {
				Log.fine("%s: 404 file was not found.", getIp());
			} catch (IOException e) {
				Log.fine("%s: Unable to read 404 file.", getIp());
			}
		}
		bytes.addAll(arrayToVector(String.format("%s%s", CRLF, CRLF).getBytes()));
		return vectorToArray(bytes);
	}

	/**
	 * TODO implement.
	 * 
	 * @return TODO
	 */
	public byte[] get400() {
		return String.format("HTTP/1.1 400 Bad Request%s", CRLF).getBytes();
	}

	private byte[] getServer() {
		return String.format("Server: %s%s", APP_NAME, CRLF).getBytes();
	}

	private byte[] getContentType() {
		return String.format("Content-Type: %s%s",
				ContentType.getInstance().getContentType(path), CRLF)
				.getBytes();
	}

	public byte[] getContentLength(int length) {
		return String.format("Content-Length: %d%s", length, CRLF).getBytes();
	}
}
