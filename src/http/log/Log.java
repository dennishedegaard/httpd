package http.log;

import http.config.Config;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * <p>
 * This logging system uses alot of the things from the java logging framework
 * (java.util.logging), the methods for logging is:
 * </p>
 * <ul>
 * <li>{@link #severe(String)} - For severe messages, for instance crashes.</li>
 * <li>{@link #warning(String)} - Something that should not happen.</li>
 * <li>{@link #info(String)} - Unusual flow in the code.</li>
 * <li>{@link #fine(String)} - Simple debugging of connections.</li>
 * </ul>
 * <p>
 * By default fine level logs are disabled, you can enable this by setting
 * another logging {@link Level}.
 * </p>
 * 
 * @author Dennis Hedegaard
 * 
 */
public class Log {
	private static Log instance;
	private volatile Logger logger;

	static {
		instance = new Log();
	}

	private Log() {
		logger = Logger.getLogger("httpd");
		logger.setLevel(Level.ALL);
		logger.setUseParentHandlers(false);
		logger.addHandler(new PrintStreamHandler(System.err, false));
		try {
			logger.addHandler(new PrintStreamHandler(new PrintStream(Config
					.getConfig().getLog()), true));
		} catch (FileNotFoundException e) {
			severe("The logging file cannot be opened for writing: "
					+ e.getMessage());
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		logger.setLevel(Config.getConfig().getLogLevel());
	}

	private Logger getLogger() {
		synchronized (this) {
			return logger;
		}
	}

	/**
	 * Sets a new logging level, this is nice for some text-based debugging. The
	 * default logginng level is {@link Level#INFO}.
	 * 
	 * @param newlevel
	 *            The new logging {@link Level} to use.
	 */
	public static void setLevel(Level newlevel) {
		instance.getLogger().setLevel(newlevel);
	}

	/**
	 * This uses the {@link Level#SEVERE} level for a message.
	 * 
	 * @param msg
	 *            The message to log.
	 */
	public static void severe(String msg) {
		instance.getLogger().severe(msg);
	}

	/**
	 * This uses the {@link Level#SEVERE} level for a message.
	 * 
	 * @param msg
	 *            The message to log.
	 * @param objs
	 *            The objects for the string formatter.
	 */
	public static void severe(String msg, Object... objs) {
		instance.getLogger().severe(String.format(msg, objs));
	}

	/**
	 * This uses the {@link Level#WARNING} level for a message.
	 * 
	 * @param msg
	 *            The message to log.
	 */
	public static void warning(String msg) {
		instance.getLogger().warning(msg);
	}

	/**
	 * This uses the {@link Level#WARNING} level for a message.
	 * 
	 * @param msg
	 *            The message to log.
	 * @param objs
	 *            The objects for the string formatter.
	 */
	public static void warning(String msg, Object... objs) {
		instance.getLogger().warning(String.format(msg, objs));
	}

	/**
	 * This uses the {@link Level#INFO} level for a message.
	 * 
	 * @param msg
	 *            The message to log.
	 */
	public static void info(String msg) {
		instance.getLogger().info(msg);
	}

	/**
	 * This uses the {@link Level#INFO} level for a message.
	 * 
	 * @param msg
	 *            The message to log.
	 * @param objs
	 *            The objects for the string formatter.
	 */
	public static void info(String msg, Object... objs) {
		instance.getLogger().info(String.format(msg, objs));
	}

	/**
	 * This uses the {@link Level#FINE} level for a message.
	 * 
	 * @param msg
	 *            The message to log.
	 */
	public static void fine(String msg) {
		instance.getLogger().fine(msg);
	}

	/**
	 * This uses the {@link Level#FINE} level for a message.
	 * 
	 * @param msg
	 *            The message to log.
	 * @param objs
	 *            The objects for the string formatter.
	 */
	public static void fine(String msg, Object... objs) {
		instance.getLogger().fine(String.format(msg, objs));
	}

	/**
	 * A simple implementation of the {@link Handler} interface, to deal with
	 * {@link PrintStream} streams. This is usefull for std outputs and file
	 * writing, which is the main concern of the logging mechanish.
	 * 
	 * @author Dennis Hedegaard
	 * 
	 */
	private static class PrintStreamHandler extends Handler {
		private static DateFormat format;
		private PrintStream stream;
		private boolean autoFlush;

		static {
			format = SimpleDateFormat.getTimeInstance();
		}

		public PrintStreamHandler(PrintStream stream, boolean autoFlush) {
			this.stream = stream;
		}

		@Override
		public void close() throws SecurityException {
			stream.close();
		}

		@Override
		public void flush() {
			stream.flush();
		}

		@Override
		public void publish(LogRecord record) {
			stream.println("[" + format.format(new Date(record.getMillis()))
					+ "] " + record.getLevel() + ": " + record.getMessage());
			if (autoFlush)
				stream.flush();
		}
	}
}
