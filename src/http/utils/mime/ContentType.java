package http.utils.mime;

import javax.activation.MimetypesFileTypeMap;

/**
 * <p>
 * This class specializes {@link MimetypesFileTypeMap} with a singleton pattern
 * using double-checked locking. This is used for getting a mime content type,
 * from a file. This method only uses the extension of a file.
 * </p>
 * <p>
 * A slower but more precise way is to use {@link java.net.URL#openConnection()}
 * for an {@link java.net.URLConnection} object. and use
 * {@link java.net.URLConnection#getContentType()}.
 * </p>
 * <p>
 * To resolve an extension to a mime, simply: {@link #getContentType(String)} or
 * {@link #getContentType(java.io.File)}.
 * </p>
 * 
 * @author Dennis Hedegaard
 * 
 */
public class ContentType extends MimetypesFileTypeMap {
	/**
	 * Double checked locking, requires <code>volatile</code>.
	 */
	private static volatile ContentType instance;

	private ContentType() {
		super();
	}

	/**
	 * Returns an instance of the object, see {@link ContentType} for more info.
	 * 
	 * @return Instance of {@link ContentType}, if one does not exist, one is
	 *         created.
	 */
	public static ContentType getInstance() {
		if (instance == null)
			synchronized (ContentType.class) {
				if (instance == null)
					instance = new ContentType();
			}
		return instance;
	}

	/**
	 * <p>
	 * Retarded overrides, since the current MIME implementation handles some
	 * file extension pretty badly atm.
	 * </p>
	 * <p>
	 * TODO: Implement a better MIME system.
	 * </p>
	 */
	@Override
	public synchronized String getContentType(String filename) {
		if (filename.endsWith(".css"))
			return "text/css";
		return super.getContentType(filename);
	}
}
