package http.utils.command;

import http.log.Log;

import java.util.Scanner;

public class Command extends Thread {
	private Scanner scan;

	public Command() {
		scan = new Scanner(System.in);
		start();
	}

	@Override
	public void run() {
		Log.info("Available commands: quit.");
		String s = null;
		while ((s = scan.nextLine()) != null) {
			s = s.trim();
			if (s.length() > 0)
				parse(s);
		}
	}

	public void parse(String in) {
		if (in.equals("quit")) {
			Log.info("Shutting down...");
			System.exit(1);
		} else {
			Log.warning("Unknown command: " + in);
		}
	}
}
