package http.config;

import static http.global.HttpGlobal.DEFAULT_CONFIG;
import static http.global.HttpGlobal.DEFAULT_HTMLDIR;
import static http.global.HttpGlobal.DEFAULT_INDEX;
import static http.global.HttpGlobal.DEFAULT_LOG;
import static http.global.HttpGlobal.DEFAULT_PORT;
import static http.global.HttpGlobal.DEFAULT_LOGLEVEL;

import http.log.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * <p>
 * This class contains methods for getting variables out of the configuration
 * file. The default configuration file is named in
 * {@link http.global.HttpGlobal#DEFAULT_CONFIG}.
 * </p>
 * <p>
 * The config file should be in xml, since it is parsed with the StAX framework.
 * </p>
 * <p>
 * This class is meant to be accessed with {@link #getConfig()} and then the
 * following methods:
 * </p>
 * <ul>
 * <li>{@link #getPort()}</li>
 * <li>{@link #getHtmldir()}</li>
 * <li>{@link #getIndex()}</li>
 * <li>{@link #getLog()}</li>
 * </ul>
 * 
 * @author Dennis Hedegaard
 * 
 */
public class Config {
	private static volatile Config instance;
	private XMLStreamReader reader;
	/**
	 * The port of the server.
	 */
	private int port;
	/**
	 * The html-dir, that serves as the root.
	 */
	private String htmldir;
	/**
	 * The index-file to use if a directory is requested.
	 */
	private String index;
	/**
	 * The log-file to log to.
	 */
	private String log;

	private Level loglevel;

	/**
	 * <p>
	 * This method should be statically imported where it is needed, it gets the
	 * config {@link Object}, which parses different settings for the server and
	 * should be accessible from anywhere, where it might be needed.
	 * </p>
	 * <p>
	 * The singleton implementation utilizes double-checked locking.
	 * </p>
	 * 
	 * @return An instance of {@link Config}, if one does not exist, one is
	 *         created.
	 */
	public static Config getConfig() {
		if (instance == null)
			synchronized (Config.class) {
				if (instance == null)
					instance = new Config();
			}
		return instance;
	}

	/**
	 * The default constructor, it sets up initial values, opens the StAX reader
	 * for the config and starts the {@link #parse()} method.
	 */
	private Config() {
		// Hardcoded defaults
		port = DEFAULT_PORT;
		index = DEFAULT_INDEX;
		htmldir = DEFAULT_HTMLDIR;
		log = DEFAULT_LOG;
		// Open the StAX stream for the xml config, if the config can be found.
		try {
			reader = XMLInputFactory.newInstance().createXMLStreamReader(
					new FileInputStream(DEFAULT_CONFIG));
		} catch (FileNotFoundException e) {
			System.err.println(String.format("Unable to read config file: %s",
					e.getMessage()));
		} catch (XMLStreamException e) {
			System.err.println(String.format(
					"There was an error in the xml: %s", e.getMessage()));
		} catch (FactoryConfigurationError e) {
			System.err.println(String.format("StaX factory exception: %s",
					e.getMessage()));
		}
		// Parse the xml config and set variables, if they can be found in the
		// stream.
		try {
			parse();
		} catch (XMLStreamException e) {
			Log.warning("Stream exception, while parsing config: %s",
					e.getMessage());
			e.printStackTrace();
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method handles parsing of the {@link XMLStreamReader} from the
	 * constructor, it is important that the reader is not <code>null</code> and
	 * is a the start of the stream.
	 * 
	 * @throws XMLStreamException
	 *             Thrown for various reasons.
	 */
	private void parse() throws XMLStreamException {
		while (reader.hasNext()) {
			reader.next();
			switch (reader.getEventType()) {
			// Find the next start element (tag)
			case XMLStreamConstants.START_ELEMENT:
				String element = reader.getLocalName();
				// read next (eventually a string in the tag)
				reader.next();
				// Parse text in the tag, depending on the tags name.
				if (element.equals("port")) // port
					// Check the port for an int.
					try {
						port = Integer.valueOf(reader.getText().trim());
					} catch (NumberFormatException e) {
						Log.warning("Unable to parse port number, perhaps it is not a number ?");
					}
				else if (element.equals("htmldir")) // htmldir
					htmldir = reader.getText().trim();
				else if (element.equals("index")) // index
					index = reader.getText().trim();
				else if (element.equals("log")) // log
					log = reader.getText().trim();
				else if (element.equals("loglevel")) { // loglevel
					String level = reader.getText().toUpperCase().trim();
					if (level.length() == 0)
						loglevel = Level.parse(DEFAULT_LOGLEVEL);
					else
						try {
							loglevel = Level.parse(level);
						} catch (IllegalArgumentException e) {
							loglevel = Level.parse(DEFAULT_LOGLEVEL);
						}
				}
			}
		}
		// A successful parse does not mean anything was parsed, just means the
		// method is done parsing the xml with StAX.
		Log.severe("Config parsed successfully: %s", DEFAULT_CONFIG);
	}

	/**
	 * Gets the port used by the server, if the port is 0, the port is randomly
	 * selected and cannot be found with this method. If no port could be parsed
	 * by the config, the port is: {@link http.global.HttpGlobal#DEFAULT_PORT}.
	 * 
	 * @return Port as an int.
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Gets the htmldir used by the server, if the htmldir could not get parsed
	 * from the config, it is by default:
	 * {@link http.global.HttpGlobal#DEFAULT_HTMLDIR}.
	 * 
	 * @return Htmldir as a {@link String}.
	 */
	public String getHtmldir() {
		return htmldir;
	}

	/**
	 * Gets the default index-file for a directory, if the index variable is not
	 * parsed, the default is chosen:
	 * {@link http.global.HttpGlobal#DEFAULT_INDEX}.
	 * 
	 * @return Index as a {@link String}.
	 */
	public String getIndex() {
		return index;
	}

	/**
	 * Gets the default log file, if the variable is not parsed, the default is
	 * chosen: {@link http.global.HttpGlobal#DEFAULT_LOG}.
	 * 
	 * @return Log as a {@link String}.
	 */
	public String getLog() {
		return log;
	}

	public Level getLogLevel() {
		return loglevel;
	}
}
