package http.global;

/**
 * TODO implement file reader etc
 * 
 * @author Dennis Hedegaard
 * 
 */
public final class HttpGlobal {
	public static final String ERROR_404 = "error_html/404.html";
	public static final String DEFAULT_INDEX = "index.html";
	/**
	 * The default port used, if no config file is present.
	 */
	public static final int DEFAULT_PORT = 8080;;
	/**
	 * The default content type to be used, if no config file is present.
	 */
	public static final String DEFAULT_CONTENT_TYPE = "text/plain";
	/**
	 * Default app name, used when sending a Server tag back to a client.
	 */
	public static final String APP_NAME = "Java HTTP Server";
	/**
	 * The default name of the config file (in xml format).
	 */
	public static final String DEFAULT_CONFIG = "http.xml";

	public static final String DEFAULT_HTMLDIR = "public_html";

	public static final String DEFAULT_LOG = "http.log";

	public static final String DEFAULT_LOGLEVEL = "info";

	/**
	 * The carriage return, line feed. Used to separate lines.
	 */
	public static final String CRLF = "\r\n";
}
